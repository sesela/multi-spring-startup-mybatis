package sample.web;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import sample.domain.service.CategoryService;
import sample.domain.service.CustomerService;
import sample.domain.service.ItemService;
import sample.model.entity.Category;
import sample.model.entity.Customer;
import sample.model.entity.Item;
import sample.model.entity.cnst.CustomerRoleEnum;
import sample.model.entity.cnst.GenderEnum;
import sample.model.entity.cnst.PrefectureEnum;

/**
 * SpringBoot起動クラス
 */
@SpringBootApplication(scanBasePackages = {"sample.web", "sample.domain", "sample.common"}) // SUPPRESS CHECKSTYLE For SpringBoot
@MapperScan("sample.domain.mapper")
public class Application {

	/**
	 * メインクラス
	 * @param args 実行引数
	 * @throws Throwable 例外
	 */
	public static void main(String[] args) throws Throwable {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * DozerBeanを作成します。
	 * @return DozerBean
	 */
	@Bean
	public Mapper dozerBean() {
		List<String> mappingFiles = Arrays.asList(
				"dozer/dozer-global-configuration.xml",
				"dozer/dozer-bean-mappings.xml",
				"dozer/more-dozer-bean-mappings.xml");
		DozerBeanMapper dozerBean = new DozerBeanMapper();
		dozerBean.setMappingFiles(mappingFiles);
		return dozerBean;
	}

	/**
	 * LocalValidatorFactoryBeanを作成します。
	 * @param messageSource MessageSource
	 * @return LocalValidatorFactoryBean
	 */
	@Bean
	public LocalValidatorFactoryBean validator(MessageSource messageSource) {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.setValidationMessageSource(messageSource);
		return localValidatorFactoryBean;
	}

	/**
	 * 起動時の初期化
	 * @return CommandLineRunner
	 */
	@Profile({"dev", "dev2"})
	@Bean
	CommandLineRunner initForDev() {
		return new CommandLineRunner() {

			@Autowired
			private CustomerService customerService;

			@Autowired
			private CategoryService categoryService;

			@Autowired
			private ItemService itemService;

			@Override
			public void run(String... args) throws Exception {
				Customer customer = new Customer(
						"loginId",
						new BCryptPasswordEncoder().encode("password"),
						"email@sample.com",
						Arrays.asList(CustomerRoleEnum.NORMAL).stream().collect(Collectors.toSet()));
				customerService.create(customer);
				customer.setLoginId("user");
				customer.setGender(GenderEnum.FEMALE);
				customer.setCustomerRoleSet(Arrays.asList(CustomerRoleEnum.NORMAL).stream().collect(Collectors.toSet()));
				customerService.update(customer);
				customer.setLastName("山田");
				customer.setFirstName("花子");
				customer.setPrefecture(PrefectureEnum.HOKKAIDO);
				customer.setCity("札幌市");
				customer.setAddressLine1("xxxxxx");
				customer.setAddressLine2("yyyyyy");
				customer.setZipCode("1230001");
				customer.setCustomerRoleSet(Arrays.asList(CustomerRoleEnum.ADMIN).stream().collect(Collectors.toSet()));
				customerService.update(customer);

				categoryService.create(new Category("c-code01", "c-name01", "c-description01"));
				categoryService.create(new Category("c-code02", "c-name02", "c-description02"));
				categoryService.create(new Category("c-code03", "c-name03", "c-description03"));
				List<Category> categoryList = categoryService.findAll();

				Item item = new Item("item-code", "item-name", "item-description",
						categoryList.stream().collect(Collectors.toSet()));
				itemService.create(item);

				item.setCategorySet(new HashSet<>());
				item.setName("item-nameUp");
				itemService.update(item);
			}
		};
	}

}


package sample.web.handler;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import lombok.RequiredArgsConstructor;
import sample.common.exception.SystemException;
import sample.web.Application;

/**
 * 例外ハンドリングクラス
 *
 */
@RestControllerAdvice(basePackageClasses = Application.class)
@RequiredArgsConstructor
public class ExceptionHandlerAdvice {

	/** ログ */
	private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);

	/** MessageSource */
	private final MessageSource messageSource;

	/**
	 * システム例外処理
	 * @param req リクエスト
	 * @param exception 例外
	 * @return 遷移先ModelAndView
	 */
	@ResponseStatus(HttpStatus.OK)
	@ExceptionHandler({SystemException.class, Exception.class})
	public ModelAndView systemError(HttpServletRequest req, Exception exception) {
		LOG.error("システム例外が発生しました。", exception);
		ModelAndView mav = new ModelAndView();
		mav.addObject("message",
				getMessage("error.SystemError.message", null, "system exception occurred."));
		mav.setViewName("error");
		return mav;
	}


	/**
	 * メッセージソースからメッセージを取得します。
	 * @param code コード
	 * @param args 引数
	 * @param defaultMessage デフォルトメッセージ
	 * @return メッセージ
	 */
	private String getMessage(String code, Object[] args, String defaultMessage) {
		return messageSource.getMessage(code, args, defaultMessage, Locale.getDefault());
	}

}

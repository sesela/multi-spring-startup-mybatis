package sample.web.helper;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import sample.domain.service.CustomerService;
import sample.model.entity.Customer;

/**
 * SpringSecurityヘルパークラス
 */
@Component
@AllArgsConstructor
public class SpringSecurityHelper {

	/** CustomerService */
	private final CustomerService customerService;

	/**
	 * 自身のログイン顧客情報を取得します。
	 * @return 顧客情報
	 */
	public Customer getMyself() {
		String loginId = SpringSecurityHelper.getMyselfAuthPrincipalCode();
		if (loginId == null) {
			return null;
		}
		return customerService.findByLonginId(loginId);
	}

	/**
	 * コンテキスト情報からログインID（プリンシパルコード）を取得します。
	 * @return ログインID
	 */
	public static String getMyselfAuthPrincipalCode() {
		String auditor = null;
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.isAuthenticated()) {
			auditor = ((UserDetails) authentication.getPrincipal()).getUsername();
		}
		return auditor;
	}
}

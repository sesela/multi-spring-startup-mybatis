package sample.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import sample.domain.audit.AuditorAware;
import sample.model.entity.Customer;
import sample.web.helper.SpringSecurityHelper;

/**
 * DB設定クラス
 */
@Configuration
public class DataAccessConfig {

	/**
	 * 監査項目を取得します。
	 * @param springSecurityHelper SpringSecurityヘルパークラス
	 * @return 監査項目
	 */
	@Bean
	AuditorAware auditorAware(SpringSecurityHelper springSecurityHelper) {
		Customer customer = springSecurityHelper.getMyself();
		return () -> customer == null ? "system" : "c-" + customer.getId();
	}
}

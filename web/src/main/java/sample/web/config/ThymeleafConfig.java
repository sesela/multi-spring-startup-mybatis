package sample.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;

/**
 * Thymeleaf設定クラス
 */
@Configuration
public class ThymeleafConfig {

	/**
	 * テンプレートエンジンを作成します。
	 * @return テンプレートエンジン
	 */
	@Bean
	public TemplateEngine templateEngin() {
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.addDialect(new Java8TimeDialect());
		return templateEngine;
	}

}

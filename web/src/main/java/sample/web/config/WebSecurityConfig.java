package sample.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import lombok.EqualsAndHashCode;
import lombok.Value;
import sample.domain.service.CustomerService;
import sample.model.entity.Customer;

/**
 * Webセキュリティ設定クラス
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/css/**", "/image/**", "/js/**", "/h2-console/**");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/customer/create").permitAll() // 認証なしでアクセスできるパスの指定
				.antMatchers("/**/register/**").hasRole("admin") // adminの場合ロールがadminの場合のみ許可する
				// その他のリクエストは認証が必要
				.anyRequest().authenticated().and().formLogin()
				// ログインフォームのパス
				.loginPage("/login").permitAll().and().logout()
				// ログアウトがパス(GET)の場合設定する（CSRF対応）ただおそらくあまり推奨されていない
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/") // ログアウト成功時の遷移先指定
				.permitAll().deleteCookies("JSESSIONID") // ログアウト時にセッションIDをクッキーから削除する
				.and().sessionManagement().invalidSessionUrl("/"); // 無効なセッションIDが指定された場合の遷移先を指定
	}

	/**
	 * 認証処理を設定します。
	 * @param auth            AuthenticationManagerBuilder
	 * @param customerService CustomerService
	 * @throws Exception 設定エラー
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth, CustomerService customerService) throws Exception {
		auth
				// ユーザー認証処理
				.userDetailsService(myUserDetailsService(customerService))
				// パスワード認証処理
				.passwordEncoder(new BCryptPasswordEncoder());
	}

	/**
	 * ユーザ情報
	 */
	@Value
	@EqualsAndHashCode(callSuper = true)
	public static class CustomUserDetails extends User {

		/** serialVersionUID */
		private static final long serialVersionUID = 1L;
		/** 姓 */
		private final String lastName;
		/** 名 */
		private final String firstName;

		/**
		 * コンストラクタ
		 * @param loginId     ログインID
		 * @param password    パスワード
		 * @param authorities 権限
		 * @param lastName    姓
		 * @param firstName   名
		 */
		public CustomUserDetails(String loginId, String password, String[] authorities,
				String lastName, String firstName) {
			super(loginId, password, AuthorityUtils.createAuthorityList(authorities));
			this.lastName = lastName;
			this.firstName = firstName;
		}
	}

	/**
	 * UserDetailsServiceを構築します。
	 *
	 * @param customerService CustomerService
	 * @return UserDetailsService
	 */
	@Bean
	public UserDetailsService myUserDetailsService(CustomerService customerService) {
		return (loginId) -> {
//			UserBuilder builder = null;
//			try {
//				Customer customer = customerService.findByLonginId(loginId);
//				builder = User.withUsername(loginId);
//				builder.password(customer.getLoginPassword());
//				// ROLE_は必ず付ける。
//				builder.roles(
//						customer.getCustomerRoleSet().stream()
//						.map(role -> "ROLE_" + role.getDbData()).toArray(String[]::new));
//			} catch (Exception noResultException) {
//				throw new UsernameNotFoundException("User not found.", noResultException);
//			}
//			return builder.build();
			CustomUserDetails user = null;
			try {
				Customer customer = customerService.findByLonginId(loginId);
				user = new CustomUserDetails(loginId, customer.getLoginPassword(),
						customer.getCustomerRoleSet().stream().map(role -> "ROLE_" + role.getDbData())
								.toArray(String[]::new),
						customer.getLastName(), customer.getFirstName());
			} catch (Exception noResultException) {
				throw new UsernameNotFoundException("User not found.", noResultException);
			}
			return user;
		};
	}
}

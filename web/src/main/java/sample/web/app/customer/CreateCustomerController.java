package sample.web.app.customer;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.dozer.Mapper;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import sample.domain.service.CustomerService;
import sample.model.entity.Customer;
import sample.model.entity.cnst.CustomerRoleEnum;

/**
 * 顧客情報登録コントローラ
 */
@Controller
@RequestMapping("customer")
@AllArgsConstructor
public class CreateCustomerController {

	/** ModelAttribute名 */
	@Getter
	private static final String CREATE_CUSTOMER_FORM_NAME = "createCustomerForm";
	/** CustomerService */
	private final CustomerService customerService;
	/** CreateCustomerFormValidator */
	private final CreateCustomerFormValidator createCustomerFormValidator;
	/** BeanMapper */
	private final Mapper beanMapper;

	/**
	 * データバインド設定
	 * @param binder WebDataBinder
	 */
	@InitBinder(CREATE_CUSTOMER_FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.addValidators(createCustomerFormValidator);
	}

	/**
	 * 顧客登録フォームを返します。
	 * @return 顧客登録フォーム
	 */
	@ModelAttribute(CREATE_CUSTOMER_FORM_NAME)
	public CreateCustomerForm setUpCreateCustomerForm() {
		CreateCustomerForm form = new CreateCustomerForm();
		return form;
	}

	/**
	 * 顧客登録入力画面
	 * @return View名
	 */
	@GetMapping(value = "create")
	public String create() {
		return "customer/create";
	}

	/**
	 * 顧客登録入力Redo画面
	 * @param form 顧客登録フォーム
	 * @return View名
	 */
	@PostMapping(value = "create", params = "redo")
	public String createRedo(@ModelAttribute(CREATE_CUSTOMER_FORM_NAME) CreateCustomerForm form) {
		return "customer/create";
	}

	/**
	 * 顧客登録入力確認画面
	 * @param form 顧客登録フォーム
	 * @param result BindingResult
	 * @return View名
	 */
	@PostMapping(value = "create", params = "confirm")
	public String createConfirm(@ModelAttribute(CREATE_CUSTOMER_FORM_NAME) @Validated CreateCustomerForm form, BindingResult result) {
		if (result.hasErrors()) {
			return createRedo(form);
		}
		return "customer/createConfirm";
	}

	/**
	 * 顧客登録処理
	 * @param form 顧客登録フォーム
	 * @param result BindingResult
	 * @param redirectAttr RedirectAttributes
	 * @return 遷移先
	 */
	@PostMapping(value = "create")
	public String create(@ModelAttribute(CREATE_CUSTOMER_FORM_NAME) @Validated CreateCustomerForm form,
			BindingResult result, RedirectAttributes redirectAttr) {
		if (result.hasErrors()) {
			return createRedo(form);
		}
		Customer customer = beanMapper.map(form, Customer.class);
		customer.setLoginPassword(new BCryptPasswordEncoder().encode(form.getLoginPassword()));
		customer.setCustomerRoleSet(Arrays.asList(CustomerRoleEnum.NORMAL).stream().collect(Collectors.toSet()));

		customerService.create(customer);
		redirectAttr.addFlashAttribute(customer);
		return "redirect:/customer/create?complete";
	}

	/**
	 * 顧客登録完了画面
	 * @return View名
	 */
	@GetMapping(value = "create", params = "complete")
	public String createComplete() {
		return "customer/createComplete";
	}
}

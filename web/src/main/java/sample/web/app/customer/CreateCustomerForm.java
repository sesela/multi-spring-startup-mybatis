package sample.web.app.customer;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.NoArgsConstructor;
import sample.common.validator.EmailFormat;

/**
 * 顧客情報登録フォーム
 */
@Data
@NoArgsConstructor
public class CreateCustomerForm implements Serializable {
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** ログインID */
	@NotEmpty
	@Length(max = 20)
	private String loginId;

	/** ログインパスワード */
	@NotEmpty
	@Length(max = 10)
	private String loginPassword;

	/** メールアドレス */
	@EmailFormat
	@NotEmpty
	private String email;
}

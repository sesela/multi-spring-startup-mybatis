package sample.web.app.customer;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import lombok.AllArgsConstructor;
import sample.domain.service.CustomerService;
import sample.model.entity.Customer;
import sample.web.helper.SpringSecurityHelper;

/**
 * UpdateCustomerFormValidator
 */
@Component
@AllArgsConstructor
public class UpdateCustomerFormValidator implements Validator {

	/** LoginIdバリデータメッセージ */
	private static final String VALIDATOR_MESSAGE_LOGIN_ID = "Duplicate.loginId.message";

	/** CustomerService */
	private CustomerService customerService;
	/** SpringSecurityUtil */
	private SpringSecurityHelper springSecurityUtil;

	@Override
	public final boolean supports(Class<?> clazz) {
		return UpdateCustomerForm.class.isAssignableFrom(clazz);
	}

	@Override
	public final void validate(@NonNull Object target, Errors errors) {
		UpdateCustomerForm form = UpdateCustomerForm.class.cast(target);
		String loginId = form.getLoginId();
		Customer customer = springSecurityUtil.getMyself();
		if (StringUtils.isEmpty(loginId)
				|| loginId.equals(customer.getLoginId())
				|| !customerService.existsLoginId(loginId)) {
			return;
		}
		errors.rejectValue("loginId", VALIDATOR_MESSAGE_LOGIN_ID,
				new String[] {loginId}, VALIDATOR_MESSAGE_LOGIN_ID);
	}
}

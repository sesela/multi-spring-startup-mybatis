package sample.web.app.customer;

import org.dozer.Mapper;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.AllArgsConstructor;
import lombok.Getter;
import sample.domain.service.CustomerService;
import sample.model.entity.Customer;
import sample.web.helper.SpringSecurityHelper;

/**
 * 顧客情報更新コントローラ
 */
@Controller
@RequestMapping("customer")
@AllArgsConstructor
public class UpdateCustomerController {

	/** ModelAttribute名 */
	@Getter
	private static final String UPDATE_CUSTOMER_FORM_NAME = "updateCustomerForm";
	/** CustomerService */
	private final CustomerService customerService;
	/** SpringSecurityHelper */
	private final SpringSecurityHelper springSecurityHelper;
	/** UpdateCustomerFormValidator */
	private final UpdateCustomerFormValidator updateCustomerFormValidator;
	/** BeanMapper */
	private final Mapper beanMapper;

	/**
	 * データバインド設定
	 * @param binder WebDataBinder
	 */
	@InitBinder(UPDATE_CUSTOMER_FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.addValidators(updateCustomerFormValidator);
	}

	/**
	 * 顧客更新フォームを返します。
	 * @return 顧客更新フォーム
	 */
	@ModelAttribute(UPDATE_CUSTOMER_FORM_NAME)
	public UpdateCustomerForm setUpUpdateCustomerForm() {
		UpdateCustomerForm form = new UpdateCustomerForm();
		return form;
	}

	/**
	 * 顧客更新入力画面
	 * @param form 顧客情報更新フォーム
	 * @return View名
	 */
	@GetMapping(value = "update")
	public String update(@ModelAttribute(UPDATE_CUSTOMER_FORM_NAME) UpdateCustomerForm form) {
		Customer customer = springSecurityHelper.getMyself();
		beanMapper.map(customer, form);
		return "customer/update";
	}

	/**
	 * 顧客更新入力Redo画面
	 * @param form 顧客情報更新フォーム
	 * @return View名
	 */
	@PostMapping(value = "update", params = "redo")
	public String updateRedo(@ModelAttribute(UPDATE_CUSTOMER_FORM_NAME) UpdateCustomerForm form) {
		return "customer/update";
	}

	/**
	 * 顧客更新入力確認画面
	 * @param form 顧客情報更新フォーム
	 * @param result BindingResult
	 * @return View名
	 */
	@PostMapping(value = "update", params = "confirm")
	public String updateConfirm(@ModelAttribute(UPDATE_CUSTOMER_FORM_NAME) @Validated UpdateCustomerForm form, BindingResult result) {
		if (result.hasErrors()) {
			return updateRedo(form);
		}
		return "customer/updateConfirm";
	}

	/**
	 * 顧客更新処理
	 * @param form 顧客情報更新フォーム
	 * @param result BindingResult
	 * @param redirectAttr RedirectAttributes
	 * @return 遷移先
	 */
	@PostMapping(value = "update")
	public String update(@ModelAttribute(UPDATE_CUSTOMER_FORM_NAME) @Validated UpdateCustomerForm form,
			BindingResult result, RedirectAttributes redirectAttr) {
		if (result.hasErrors()) {
			return updateRedo(form);
		}
		Customer customer = springSecurityHelper.getMyself();
		beanMapper.map(form, customer);
		customerService.update(customer);
		return "redirect:/customer/update?complete";
	}

	/**
	 * 顧客更新完了画面
	 * @return View名
	 */
	@GetMapping(value = "update", params = "complete")
	public String updateComplete() {
		return "customer/updateComplete";
	}
}

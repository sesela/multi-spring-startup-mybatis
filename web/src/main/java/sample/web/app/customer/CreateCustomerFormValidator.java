package sample.web.app.customer;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import lombok.AllArgsConstructor;
import sample.domain.service.CustomerService;

/**
 * CreateCustomerFormValidator
 */
@Component
@AllArgsConstructor
public class CreateCustomerFormValidator implements Validator {

	/** LoginIdバリデータメッセージ */
	private static final String VALIDATOR_MESSAGE_LOGIN_ID = "Duplicate.loginId.message";

	/** CustomerService */
	private CustomerService customerService;

	@Override
	public final boolean supports(Class<?> clazz) {
		return CreateCustomerForm.class.isAssignableFrom(clazz);
	}

	@Override
	public final void validate(@NonNull Object target, Errors errors) {
		CreateCustomerForm form = CreateCustomerForm.class.cast(target);
		String loginId = form.getLoginId();
		if (StringUtils.isEmpty(loginId) || !customerService.existsLoginId(loginId)) {
			return;
		}
		errors.rejectValue("loginId", VALIDATOR_MESSAGE_LOGIN_ID,
				new String[] {loginId}, VALIDATOR_MESSAGE_LOGIN_ID);
	}
}

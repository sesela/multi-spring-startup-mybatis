package sample.web.app.customer;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.Data;
import lombok.NoArgsConstructor;
import sample.common.validator.EmailFormat;
import sample.model.entity.cnst.GenderEnum;

/**
 * 顧客情報更新フォーム
 */
@Data
@NoArgsConstructor
public class UpdateCustomerForm implements Serializable {
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** ログインID */
	@NotEmpty
	@Length(max = 20)
	private String loginId;

	/** メールアドレス */
	@EmailFormat
	@NotEmpty
	private String email;

	/** 姓 */
	@Length(min = 1, max = 20)
	private String lastName;

	/** 名 */
	@Length(min = 1, max = 20)
	private String firstName;

	/** 性別 */
	private GenderEnum gender;

//	/** 都道府県 */
//	private String prefecture;
//
//	/** 市町村 */
//	@Size(min = 1, max = 20)
//	private String city;
//
//	/** 住所 */
//	@Size(min = 1, max = 100)
//	private String addressLine1;
//
//	/** マンション・アパート名等 */
//	@Size(min = 1, max = 100)
//	private String addressLine2;
//
//	/** 郵便番号 */
//	@Size(min = 1, max = 10)
//	private String zipCode;
//
//	/** 連絡先種別 */
//	private String phoneType;
//
//	/** 連絡先 */
//	@Size(min = 1, max = 10)
//	private String phoneNo;

}

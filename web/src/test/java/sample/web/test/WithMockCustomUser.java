package sample.web.test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.security.test.context.support.WithSecurityContext;

/**
 * Custom @WithMockUser
 */
@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockCustomUserSecurityContextFactory.class)
public @interface WithMockCustomUser {
	String username() default "user";
	String password() default "password";
	String lastName() default "yamada";
	String firstName() default "taro";
	String email() default "user@example.com";
	String[] roles() default { "USER" };
}
package sample.web.config;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import sample.web.test.WithMockCustomUser;

@ExtendWith(SpringExtension.class)
class MvcConfigTest {

	@Nested
	@DisplayName("トップページ")
	@SpringBootTest
	@ActiveProfiles({"test"})
	@AutoConfigureMockMvc
	class TopPageTest {
		@Autowired
		private MockMvc mockMvc;

		@Test
		@DisplayName("ログイン済み")
//		@WithMockUser(username = "user")
		@WithMockCustomUser(username = "user")
		void valid() throws Exception {
			mockMvc.perform(get("/top"))
			.andExpect(status().isOk())
			.andExpect(view().name("top"))
			.andExpect(model().hasNoErrors());
		}

		@Test
		@DisplayName("未ログイン")
		void noLogin() throws Exception {
			mockMvc.perform(get("/top"))
			.andExpect(redirectedUrl("http://localhost/login"))
			.andExpect(status().isFound());
		}	
	}

	@Nested
	@DisplayName("ルートページ")
	@SpringBootTest
	@ActiveProfiles({"test"})
	@AutoConfigureMockMvc
	class RootTest {
		@Autowired
		private MockMvc mockMvc;

		@Test
		@DisplayName("ログイン済み")
//		@WithMockUser(username = "user")
		@WithMockCustomUser(username = "user")
		void valid() throws Exception {
			mockMvc.perform(get("/"))
			.andExpect(status().isFound())
			.andExpect(redirectedUrl("/top"));
		}

		@Test
		@DisplayName("未ログイン")
		void noLogin() throws Exception {
			mockMvc.perform(get("/"))
			.andExpect(redirectedUrl("/top"))
			.andExpect(status().isFound());
		}	
	}

}

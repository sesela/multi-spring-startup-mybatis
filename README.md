## CI最新結果
- master　[![pipeline status](https://gitlab.com/sesela/multi-spring-startup-mybatis/badges/master/pipeline.svg)](https://gitlab.com/sesela/multi-spring-startup-mybatis/commits/master) [![coverage report](https://gitlab.com/sesela/multi-spring-startup-mybatis/badges/master/coverage.svg)](https://gitlab.com/sesela/multi-spring-startup-mybatis/commits/master)
- develop　[![pipeline status](https://gitlab.com/sesela/multi-spring-startup-mybatis/badges/develop/pipeline.svg)](https://gitlab.com/sesela/multi-spring-startup-mybatis/commits/develop) [![coverage report](https://gitlab.com/sesela/multi-spring-startup-mybatis/badges/develop/coverage.svg)](https://gitlab.com/sesela/multi-spring-startup-mybatis/commits/develop)


## モジュール生成
### WEBモジュール生成
```sh
master-sample-web/gradlew -b master-sample-web/build.gradle clean build
```

テストとJavaDoc生成とビルドを行います。
- 「master-sample-web/build/libs」直下にwarが生成されます。
- 「master-sample-web/build/reports」直下にJunitテスト結果レポートとカバレッジレポートが生成されます。
- 「master-sample-web/build/javadoc」直下にjavadocが生成されます。

## テスト
### JUnitテスト結果レポート生成
```sh
master-sample-web/gradlew -b master-sample-web/build.gradle clean testReport
```

「master-sample-web/build/reports/allTests」にレポートが生成されます。

### JUnitテスト実施時のカバレッジレポート生成
```sh
master-sample-web/gradlew -b master-sample-web/build.gradle clean jacocoMergedReport
```

「master-sample-web/build/reports/jacoco」にレポートが生成されます。

## javadoc
### javadoc生成
```sh
master-sample-web/gradlew -b master-sample-web/build.gradle clean javadocAllProjets
```

「master-sample-web/build/javadoc」にjavadocが生成されます。


package sample.common.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.UnexpectedTypeException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

class LongFormatValidatorTest {

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ValideTestDataType {
		@LongFormat
		public String value;
	}
	
	@Data
	@AllArgsConstructor
	public static class InvalidTestDataType {
		@LongFormat
		public Long value;
	}

	@Test
	@DisplayName("正常値(Long.MAX_VALUE)の場合はバリデーションエラーとならないこと。")
	void validMaxFormat() {
		ValideTestDataType data = new ValideTestDataType(String.valueOf(Long.MAX_VALUE));
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}
	
	@Test
	@DisplayName("正常値(Long.MIN_VALUE)の場合はバリデーションエラーとならないこと。")
	void validMinFormat() {
		ValideTestDataType data = new ValideTestDataType(String.valueOf(Long.MIN_VALUE));
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}

	@Test
	@DisplayName("正常値(null)の場合はバリデーションエラーとならないこと。")
	void validNullFormat() {
		ValideTestDataType data = new ValideTestDataType();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}

	@Test
	@DisplayName("異常値(xxxxx)の場合はバリデーションエラーとなること。")
	void invalidFormat() {
		ValideTestDataType data = new ValideTestDataType("xxxxx");
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.iterator().next().getMessage()).isEqualTo("{commons.validation.LongFormat.message}");
		assertThat(result.isEmpty()).isFalse();
	}

	@Test
	@DisplayName("データ型がString以外(String)の場合はUnexpectedTypeException例外が発生すること。")
	void invalidType() {
		InvalidTestDataType data = new InvalidTestDataType(4452511320043047L);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		assertThrows(UnexpectedTypeException.class, ()->validator.validate(data));
	}

}

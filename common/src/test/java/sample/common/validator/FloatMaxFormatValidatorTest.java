package sample.common.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.UnexpectedTypeException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

class FloatMaxFormatValidatorTest {

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ValideTestDataType {
		@FloatMaxFormat(max = 1)
		public String value;
	}
	
	@Data
	@AllArgsConstructor
	public static class InvalidTestDataType {
		@FloatMaxFormat(max = 1)
		public Long value;
	}

	@Test
	@DisplayName("正常値(1)の場合はバリデーションエラーとならないこと。")
	void validFormat() {
		ValideTestDataType data = new ValideTestDataType("1");
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}

	@Test
	@DisplayName("正常値(null)の場合はバリデーションエラーとならないこと。")
	void validNullFormat() {
		ValideTestDataType data = new ValideTestDataType();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}

	@Test
	@DisplayName("異常値(1.01)の場合はバリデーションエラーとなること。")
	void invalidFormat() {
		try {
		ValideTestDataType data = new ValideTestDataType("1.01");
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.iterator().next().getMessage()).isEqualTo("{commons.validation.FloatMaxFormat.message}");
		assertThat(result.isEmpty()).isFalse();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("データ型がString以外(String)の場合はUnexpectedTypeException例外が発生すること。")
	void invalidType() {
		InvalidTestDataType data = new InvalidTestDataType(4452511320043047L);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		assertThrows(UnexpectedTypeException.class, ()->validator.validate(data));
	}

}

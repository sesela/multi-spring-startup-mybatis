package sample.common.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.UnexpectedTypeException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

class UrlFormatValidatorTest {

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ValideTestDataType {
		@UrlFormat
		public String value;
	}
	
	@Data
	@AllArgsConstructor
	public static class InvalidTestDataType {
		@UrlFormat
		public Long value;
	}

	@Test
	@DisplayName("正常値(https://sample.com)の場合はバリデーションエラーとならないこと。")
	void validFormat() {
		ValideTestDataType data = new ValideTestDataType("https://sample.com");
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}

	@Test
	@DisplayName("正常値(null)の場合はバリデーションエラーとならないこと。")
	void validNullFormat() {
		ValideTestDataType data = new ValideTestDataType();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}

	@Test
	@DisplayName("異常値(sample.com)の場合はバリデーションエラーとなること。")
	void invalidFormat() {
		ValideTestDataType data = new ValideTestDataType("sample.com");
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.iterator().next().getMessage()).isEqualTo("{commons.validation.UrlFormat.message}");
		assertThat(result.isEmpty()).isFalse();
	}

	@Test
	@DisplayName("データ型がString以外(String)の場合はUnexpectedTypeException例外が発生すること。")
	void invalidType() {
		InvalidTestDataType data = new InvalidTestDataType(1L);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		assertThrows(UnexpectedTypeException.class, ()->validator.validate(data));
	}

}

package sample.common.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.UnexpectedTypeException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

class CreditCardValidatorTest {

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class TestData {
		@CreditCardFormat
		public String value;
	}
	
	@Data
	@AllArgsConstructor
	public static class InvalidTestDataType {
		@CreditCardFormat
		public Long value;
	}

	@Test
	@DisplayName("正常値(4452511320043046)の場合はバリデーションエラーとならないこと。")
	void validFormat() {
		TestData data = new TestData("4452511320043046");
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}
	
	@Test
	@DisplayName("正常値(null)の場合はバリデーションエラーとならないこと。")
	void validNullFormat() {
		TestData data = new TestData();
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.isEmpty()).isTrue();
	}

	@Test
	@DisplayName("異常値(4452511320043047)の場合はバリデーションエラーとなること。")
	void invalidFormat() {
		TestData data = new TestData("4452511320043047");
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<Object>> result = validator.validate(data);
		assertThat(result.iterator().next().getMessage()).isEqualTo("{commons.validation.CreditCardFormat.message}");
		assertThat(result.isEmpty()).isFalse();
	}

	@Test
	@DisplayName("データ型がString以外(String)の場合はUnexpectedTypeException例外が発生すること。")
	void invalidType() {
		InvalidTestDataType data = new InvalidTestDataType(4452511320043047L);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		assertThrows(UnexpectedTypeException.class, ()->validator.validate(data));
	}

}

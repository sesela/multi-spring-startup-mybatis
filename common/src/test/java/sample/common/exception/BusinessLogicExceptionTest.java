package sample.common.exception;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class BusinessLogicExceptionTest {

	/**
	 * ビジネスロジック例外テスト
	 */
	@Test
	public void testBusinessLogicException() {
		try {
			throw new BusinessLogicException();
		} catch (RuntimeException e) {
			assertThat(e.getMessage()).isNull();
			;
			assertThat(e.getCause()).isNull();
			assertThat(e.getStackTrace().length).isNotEqualTo(0);
			Throwable throwableA = new Throwable();
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			assertSuppressed(e, throwableA, throwableA, throwableA);
		}
	}

	/**
	 * ビジネスロジック例外テスト
	 */
	@Test
	public void testBusinessLogicExceptionStringThrowableBooleanBoolean() {
		boolean enableSuppression = false;
		boolean writableStackTrace = false;

		enableSuppression = true;
		writableStackTrace = true;
		try {
			throw new BusinessLogicException("message1", new Throwable("Root1"), enableSuppression, writableStackTrace);
		} catch (RuntimeException e) {
			assertThat(e.getMessage()).isEqualTo("message1");
			assertThat(e.getCause().getMessage()).isEqualTo("Root1");
			assertThat(e.getStackTrace().length).isNotEqualTo(0);
			Throwable throwableA = new Throwable();
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			assertSuppressed(e, throwableA, throwableA, throwableA);
		}

		enableSuppression = false;
		writableStackTrace = true;
		try {
			throw new BusinessLogicException("message2", new Throwable("Root2"), enableSuppression, writableStackTrace);
		} catch (RuntimeException e) {
			assertThat(e.getMessage()).isEqualTo("message2");
			assertThat(e.getCause().getMessage()).isEqualTo("Root2");
			assertThat(e.getStackTrace().length).isNotEqualTo(0);
			assertSuppressed(e);
			e.addSuppressed(new Throwable());
			assertSuppressed(e);
			e.addSuppressed(new Throwable());
			assertSuppressed(e);
		}

		enableSuppression = true;
		writableStackTrace = false;
		try {
			throw new BusinessLogicException("message3", new Throwable("Root3"), enableSuppression, writableStackTrace);
		} catch (RuntimeException e) {
			assertThat(e.getMessage()).isEqualTo("message3");
			assertThat(e.getCause().getMessage()).isEqualTo("Root3");
			assertThat(e.getStackTrace().length).isEqualTo(0);
			Throwable throwableB = new Throwable();
			e.addSuppressed(throwableB);
			e.addSuppressed(throwableB);
			e.addSuppressed(throwableB);
			assertSuppressed(e, throwableB, throwableB, throwableB);
		}
	}

	/**
	 * ビジネスロジック例外テスト
	 */
	@Test
	public void testBusinessLogicExceptionStringThrowable() {
		try {
			throw new BusinessLogicException("message1", new Throwable("Root1"));
		} catch (RuntimeException e) {
			assertThat(e.getMessage()).isEqualTo("message1");
			assertThat(e.getCause().getMessage()).isEqualTo("Root1");
			assertThat(e.getStackTrace().length).isNotEqualTo(0);
			Throwable throwableA = new Throwable();
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			assertSuppressed(e, throwableA, throwableA, throwableA);
		}
	}

	/**
	 * ビジネスロジック例外テスト
	 */
	@Test
	public void testBusinessLogicExceptionString() {
		try {
			throw new BusinessLogicException("message1");
		} catch (RuntimeException e) {
			assertThat(e.getMessage()).isEqualTo("message1");
			assertThat(e.getCause()).isNull();
			;
			assertThat(e.getStackTrace().length).isNotEqualTo(0);
			Throwable throwableA = new Throwable();
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			assertSuppressed(e, throwableA, throwableA, throwableA);
		}
	}

	/**
	 * ビジネスロジック例外テスト
	 */
	@Test
	public void testBusinessLogicExceptionThrowable() {
		try {
			throw new BusinessLogicException(new Throwable("Root1"));
		} catch (RuntimeException e) {
			assertThat(e.getMessage()).isEqualTo(e.getCause().toString());
			assertThat(e.getCause().getMessage()).isEqualTo("Root1");
			assertThat(e.getStackTrace().length).isNotEqualTo(0);
			Throwable throwableA = new Throwable();
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			e.addSuppressed(throwableA);
			assertSuppressed(e, throwableA, throwableA, throwableA);
		}
	}

	/**
	 * ビジネスロジック例外テスト
	 * 
	 * @param throwable          throwable
	 * @param expectedSuppressed expectedSuppressed
	 */
	private void assertSuppressed(Throwable throwable, Throwable... expectedSuppressed) {
		assertThat(Arrays.asList(throwable.getSuppressed())).isEqualTo(Arrays.asList(expectedSuppressed));
	}

}

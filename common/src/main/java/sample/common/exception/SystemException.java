package sample.common.exception;

/**
 * 業務例外クラス
 *
 */
public class SystemException extends RuntimeException {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * コンストラクタ
	 */
	public SystemException() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param message 詳細メッセージ
	 * @param cause 原因
	 * @param enableSuppression 抑制を有効化するか、それとも無効化するか
	 * @param writableStackTrace スタックトレースを書き込み可能にするかどうか
	 */
	public SystemException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * コンストラクタ
	 * @param message 詳細メッセージ
	 * @param cause 原因
	 */
	public SystemException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * コンストラクタ
	 * @param message 詳細メッセージ
	 */
	public SystemException(String message) {
		super(message);
	}

	/**
	 * コンストラクタ
	 * @param cause 原因
	 */
	public SystemException(Throwable cause) {
		super(cause);
	}

}

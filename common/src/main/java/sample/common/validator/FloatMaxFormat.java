package sample.common.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import sample.common.validator.impl.FloatMaxFormatValidator;

/**
 * Float型最大値指定書式バリデーション用のアノテーションです。
 *
 */
@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = FloatMaxFormatValidator.class)
@Documented
public @interface FloatMaxFormat {

	/**
	 * エラーメッセージコード。デフォルト"{commons.validation.FloatMaxFormat.message}"です。
	 * @return エラーメッセージコード
	 */
	String message() default "{commons.validation.FloatMaxFormat.message}";

	/**
	 * 最大値。
	 * @return 最大値
	 */
	float max();

	/**
	 * 制約に対するバリデーションが属するグループ。
	 * @return グループ
	 */
	Class<?>[] groups() default { };

	/**
	 * ペイロード
	 * @return ペイロード
	 */
	Class<? extends Payload>[] payload() default { };


}

package sample.common.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import sample.common.validator.impl.FloatMinFormatValidator;

/**
 * Float型最小値指定書式バリデーション用のアノテーションです。
 *
 */
@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = FloatMinFormatValidator.class)
@Documented
public @interface FloatMinFormat {

	/**
	 * エラーメッセージコード。デフォルト"{commons.validation.FloatMinFormat.message}"です。
	 * @return エラーメッセージコード
	 */
	String message() default "{commons.validation.FloatMinFormat.message}";

	/**
	 * 最小値。
	 * @return 最小値
	 */
	float min();

	/**
	 * 制約に対するバリデーションが属するグループ。
	 * @return グループ
	 */
	Class<?>[] groups() default { };

	/**
	 * ペイロード
	 * @return ペイロード
	 */
	Class<? extends Payload>[] payload() default { };


}

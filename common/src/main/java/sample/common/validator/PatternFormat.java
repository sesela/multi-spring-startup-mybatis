package sample.common.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import sample.common.validator.impl.PatternFormatValidator;

/**
 * パターン書式バリデーション用のアノテーションです。
 *
 */
@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = PatternFormatValidator.class)
@Documented
public @interface PatternFormat {

	/**
	 * エラーメッセージコード。デフォルト"{commons.validation.PatternFormat.message}"です。
	 * @return エラーメッセージコード
	 */
	String message() default "{commons.validation.PatternFormat.message}";

	/**
	 * チェックする正規表現。
	 * @return 正規表現
	 */
	String regex();

	/**
	 * 制約に対するバリデーションが属するグループ。
	 * @return グループ
	 */
	Class<?>[] groups() default { };

	/**
	 * ペイロード
	 * @return ペイロード
	 */
	Class<? extends Payload>[] payload() default { };

}

package sample.common.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import sample.common.validator.impl.DoubleRangeFormatValidator;

/**
 * Double型範囲指定書式バリデーション用のアノテーションです。
 *
 */
@Target({ FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = DoubleRangeFormatValidator.class)
@Documented
public @interface DoubleRangeFormat {

	/**
	 * エラーメッセージコード。デフォルト"{commons.validation.DoubleRangeFormat.message}"です。
	 * @return エラーメッセージコード
	 */
	String message() default "{commons.validation.DoubleRangeFormat.message}";

	/**
	 * 最小値。
	 * @return 最小値
	 */
	double min();

	/**
	 * 最大値。
	 * @return 最大値
	 */
	double max();

	/**
	 * 制約に対するバリデーションが属するグループ。
	 * @return グループ
	 */
	Class<?>[] groups() default { };

	/**
	 * ペイロード
	 * @return ペイロード
	 */
	Class<? extends Payload>[] payload() default { };


}

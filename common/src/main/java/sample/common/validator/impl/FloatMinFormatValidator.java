package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.FloatMinFormat;

/**
 * Float型最小値書式バリデーションクラスです。
 *
 */
public class FloatMinFormatValidator implements
		ConstraintValidator<FloatMinFormat, String> {

	/** 最小値 */
	private double min;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		float val;
		try {
			val = Float.parseFloat(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.minValue(val, min);
	}

	@Override
	public final void initialize(final FloatMinFormat arg) {
		min = arg.min();
	}

}

package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.IntMaxFormat;

/**
 * Int型最大値書式バリデーションクラスです。
 *
 */
public class IntMaxFormatValidator implements
		ConstraintValidator<IntMaxFormat, String> {

	/** 最大値 */
	private int max;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		int val;
		try {
			val = Integer.parseInt(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.maxValue(val, max);
	}

	@Override
	public final void initialize(final IntMaxFormat arg) {
		max = arg.max();
	}

}

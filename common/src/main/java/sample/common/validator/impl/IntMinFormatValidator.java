package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.IntMinFormat;

/**
 * Int型最小値書式バリデーションクラスです。
 *
 */
public class IntMinFormatValidator implements
		ConstraintValidator<IntMinFormat, String> {

	/** 最小値 */
	private int min;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		int val;
		try {
			val = Integer.parseInt(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.minValue(val, min);
	}

	@Override
	public final void initialize(final IntMinFormat arg) {
		min = arg.min();
	}

}

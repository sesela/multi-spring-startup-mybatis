package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.DoubleMaxFormat;

/**
 * Double型最大値書式バリデーションクラスです。
 *
 */
public class DoubleMaxFormatValidator implements
		ConstraintValidator<DoubleMaxFormat, String> {

	/** 最大値 */
	private double max;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		double val;
		try {
			val = Double.parseDouble(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.maxValue(val, max);
	}

	@Override
	public final void initialize(final DoubleMaxFormat arg) {
		max = arg.max();
	}

}

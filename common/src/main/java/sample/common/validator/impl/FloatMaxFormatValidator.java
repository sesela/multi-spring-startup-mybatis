package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.FloatMaxFormat;

/**
 * Float型最大値書式バリデーションクラスです。
 *
 */
public class FloatMaxFormatValidator implements
		ConstraintValidator<FloatMaxFormat, String> {

	/** 最大値 */
	private float max;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		float val;
		try {
			val = Float.parseFloat(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.maxValue(val, max);
	}

	@Override
	public final void initialize(final FloatMaxFormat arg) {
		max = arg.max();
	}

}

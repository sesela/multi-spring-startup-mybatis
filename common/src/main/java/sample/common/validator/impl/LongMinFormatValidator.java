package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.LongMinFormat;

/**
 * Long型最小値書式バリデーションクラスです。
 *
 */
public class LongMinFormatValidator implements
		ConstraintValidator<LongMinFormat, String> {

	/** 最小値 */
	private long min;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		long val;
		try {
			val = Long.parseLong(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.minValue(val, min);
	}

	@Override
	public final void initialize(final LongMinFormat arg) {
		min = arg.min();
	}

}

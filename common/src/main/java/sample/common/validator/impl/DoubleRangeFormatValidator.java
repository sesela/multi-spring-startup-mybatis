package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.DoubleRangeFormat;

/**
 * Double型範囲指定書式バリデーションクラスです。
 *
 */
public class DoubleRangeFormatValidator implements
		ConstraintValidator<DoubleRangeFormat, String> {

	/** 最小値 */
	private double min;

	/** 最大値 */
	private double max;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		double val;
		try {
			val = Double.parseDouble(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.isInRange(val, min, max);
	}

	@Override
	public final void initialize(final DoubleRangeFormat arg) {
		min = arg.min();
		max = arg.max();
	}

}

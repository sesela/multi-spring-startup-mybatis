package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.FloatRangeFormat;

/**
 * Float型範囲指定書式バリデーションクラスです。
 *
 */
public class FloatRangeFormatValidator implements
		ConstraintValidator<FloatRangeFormat, String> {

	/** 最小値 */
	private float min;

	/** 最大値 */
	private float max;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		double val;
		try {
			val = Float.parseFloat(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.isInRange(val, min, max);
	}

	@Override
	public final void initialize(final FloatRangeFormat arg) {
		min = arg.min();
		max = arg.max();
	}

}

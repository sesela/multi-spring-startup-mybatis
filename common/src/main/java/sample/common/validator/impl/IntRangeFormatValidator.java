package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.IntRangeFormat;

/**
 * Int型範囲指定書式バリデーションクラスです。
 *
 */
public class IntRangeFormatValidator implements
		ConstraintValidator<IntRangeFormat, String> {

	/** 最小値 */
	private int min;

	/** 最大値 */
	private int max;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		int val;
		try {
			val = Integer.parseInt(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.isInRange(val, min, max);
	}

	@Override
	public final void initialize(final IntRangeFormat arg) {
		min = arg.min();
		max = arg.max();
	}

}

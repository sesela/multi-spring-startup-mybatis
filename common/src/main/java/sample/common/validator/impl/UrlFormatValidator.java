package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.UrlFormat;

/**
 * URL書式バリデーションクラスです。
 *
 */
public class UrlFormatValidator implements
		ConstraintValidator<UrlFormat, String> {

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}
		return GenericValidator.isUrl(obj);
	}

	@Override
	public void initialize(final UrlFormat arg) {
	}

}

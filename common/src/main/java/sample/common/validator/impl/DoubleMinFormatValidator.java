package sample.common.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;

import sample.common.validator.DoubleMinFormat;

/**
 * Double型最小値書式バリデーションクラスです。
 *
 */
public class DoubleMinFormatValidator implements
		ConstraintValidator<DoubleMinFormat, String> {

	/** 最小値 */
	private double min;

	@Override
	public final boolean isValid(final String obj, final ConstraintValidatorContext arg1) {
		if (StringUtils.isEmpty(obj)) {
			return true;
		}

		double val;
		try {
			val = Double.parseDouble(obj);
		} catch (NumberFormatException e) {
			return true;
		}
		return GenericValidator.minValue(val, min);
	}

	@Override
	public final void initialize(final DoubleMinFormat arg) {
		min = arg.min();
	}

}

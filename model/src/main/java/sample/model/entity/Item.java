package sample.model.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 商品情報
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Item implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** 商品コード */
	@NotEmpty
	@Size(max = 20)
	@Setter(value = AccessLevel.PRIVATE)
	private String code;

	/** 商品名 */
	@NotEmpty
	@Size(max = 20)
	private String name;

	/** 内容 */
	@NotEmpty
	@Size(max = 255)
	private String description;

	/** カテゴリ */
	private Set<Category> categorySet = new HashSet<>();

	/**
	 * コンストラクタ
	 * @param code 商品コード
	 * @param name 商品名
	 * @param description 内容
	 * @param categorySet カテゴリ
	 */
	public Item(String code, String name, String description, Set<Category> categorySet) {
		this.code = code;
		this.name = name;
		this.description = description;
		this.categorySet = categorySet;
	}
}

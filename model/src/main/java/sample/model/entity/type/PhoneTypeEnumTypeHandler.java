package sample.model.entity.type;

import org.apache.ibatis.type.MappedTypes;

import sample.model.entity.cnst.PhoneTypeEnum;

/**
 * PhoneTypeEnumのTypeHandlerクラス
 */
@MappedTypes(PhoneTypeEnum.class)
public class PhoneTypeEnumTypeHandler extends AbstractEnumTypeHandler<PhoneTypeEnum> {
	/**
	 * コンストラクタ
	 */
	public PhoneTypeEnumTypeHandler() {
		super(PhoneTypeEnum.class);
	}
}

package sample.model.entity.type;

import org.apache.ibatis.type.MappedTypes;

import sample.model.entity.cnst.PrefectureEnum;

/**
 * PrefectureEnumのTypeHandlerクラス
 */
@MappedTypes(PrefectureEnum.class)
public class PrefectureEnumTypeHandler extends AbstractEnumTypeHandler<PrefectureEnum> {
	/**
	 * コンストラクタ
	 */
	public PrefectureEnumTypeHandler() {
		super(PrefectureEnum.class);
	}
}

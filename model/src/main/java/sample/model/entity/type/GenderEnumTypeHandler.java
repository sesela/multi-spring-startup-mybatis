package sample.model.entity.type;

import org.apache.ibatis.type.MappedTypes;

import sample.model.entity.cnst.GenderEnum;

/**
 * GenderEnumのTypeHandlerクラス
 */
@MappedTypes(GenderEnum.class)
public class GenderEnumTypeHandler extends AbstractEnumTypeHandler<GenderEnum> {
	/**
	 * コンストラクタ
	 */
	public GenderEnumTypeHandler() {
		super(GenderEnum.class);
	}
}

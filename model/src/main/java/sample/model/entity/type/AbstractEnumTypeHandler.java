package sample.model.entity.type;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import lombok.AllArgsConstructor;
import sample.model.entity.cnst.EntityEnum;

/**
 * Enum用TypeHandler抽象クラス
 *
 * @param <X> Enum型
 */
@AllArgsConstructor
public abstract class AbstractEnumTypeHandler<X extends Enum<X> & EntityEnum<String, X>> extends BaseTypeHandler<X> {

	/** class */
	private Class<X> clazz;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, X parameter, JdbcType jdbcType) throws SQLException {
		ps.setString(i, parameter.getDbData());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public X getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return EntityEnum.getEnum(clazz, rs.getString(columnName));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public X getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return EntityEnum.getEnum(clazz, rs.getString(columnIndex));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public X getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return EntityEnum.getEnum(clazz, cs.getString(columnIndex));
	}
}

package sample.model.entity.type;

import org.apache.ibatis.type.MappedTypes;

import sample.model.entity.cnst.CustomerRoleEnum;

/**
 * PrefectureEnumのTypeHandlerクラス
 */
@MappedTypes(CustomerRoleEnum.class)
public class CustomerRoleEnumTypeHandler extends AbstractEnumTypeHandler<CustomerRoleEnum> {
	/**
	 * コンストラクタ
	 */
	public CustomerRoleEnumTypeHandler() {
		super(CustomerRoleEnum.class);
	}
}

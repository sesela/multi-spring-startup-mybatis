package sample.model.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sample.common.validator.EmailFormat;
import sample.model.entity.cnst.CustomerRoleEnum;
import sample.model.entity.cnst.GenderEnum;
import sample.model.entity.cnst.PhoneTypeEnum;
import sample.model.entity.cnst.PrefectureEnum;

/**
 * 顧客情報
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Customer implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** ID */
	@Setter(value = AccessLevel.PRIVATE)
	private Long id;

	/** ログインID */
	@NotEmpty
	@Size(max = 20)
	private String loginId;

	/** ログインパスワード */
	@NotEmpty
	@Size(max = 255)
	private String loginPassword;

	/** メールアドレス */
	@EmailFormat
	@NotEmpty
	@Size(max = 100)
	private String email;

	/** 姓 */
	@Size(min = 1, max = 20)
	private String lastName;

	/** 名 */
	@Size(min = 1, max = 20)
	private String firstName;

	/** 性別 */
	private GenderEnum gender;

	/** 都道府県 */
	private PrefectureEnum prefecture;

	/** 市町村 */
	@Size(min = 1, max = 20)
	private String city;

	/** 住所 */
	@Size(min = 1, max = 100)
	private String addressLine1;

	/** マンション・アパート名等 */
	@Size(min = 1, max = 100)
	private String addressLine2;

	/** 郵便番号 */
	@Size(min = 1, max = 10)
	private String zipCode;

	/** 連絡先種別 */
	private PhoneTypeEnum phoneType;

	/** 連絡先 */
	@Size(min = 1, max = 10)
	private String phoneNo;

	/** 権限 */
	@EqualsAndHashCode.Exclude
	private Set<CustomerRoleEnum> customerRoleSet = new HashSet<>();

	/**
	 * コンストラクタ
	 * @param loginId ログインID
	 * @param loginPasword ログインパスワード
	 * @param email メールアドレス
	 * @param customerRoleSet 権限
	 */
	public Customer(String loginId, String loginPasword, String email, Set<CustomerRoleEnum> customerRoleSet) {
		this.loginId = loginId;
		this.loginPassword = loginPasword;
		this.email = email;
		this.customerRoleSet = customerRoleSet;
	}
}

package sample.model.entity.cnst;

import java.util.Arrays;

/**
 * Entity定数インターフェース
 * @param <T> DB値の型
 * @param <E> 本インターフェースの実装Enumの型
 */
public interface EntityEnum<T, E extends Enum<E> & EntityEnum<T, E>> {

	/**
	 * DB値取得
	 * @return DB値
	 */
	T getDbData();

	/**
	 *  [デフォルト実装] Enumに変換する
	 * @return 本インターフェースの実装Enum
	 */
	@SuppressWarnings("unchecked")
	default E toEnum() {
		return (E) this;
	}

	/**
	 * [デフォルト実装] DB値が同一かどうかをチェックする
	 * @param dbData DB値
	 * @return チェック結果
	 */
	default boolean equalsByDbData(T dbData) {
		return getDbData().equals(dbData);
	}

	/**
	 * メッセージコードを取得します。
	 * @return メッセージコード
	 */
	default String getMessageCode() {
		return getClass().getCanonicalName() + "." + toString() + ".message";
	}

	/**
	 * [staticメソッド] 本インターフェースを実装した指定されたEnumの指定されたDB値の列挙子を返却する
	 *
	 * @param clazz 本インターフェースを実装した指定されたEnum
	 * @param dbData 本インターフェースを実装した指定されたEnumで定義されているDB値
	 * @param <T> DB値の型
	 * @param <E> 本インターフェースの実装Enumの型
	 * @return 指定されたDB値に紐づく実装Enum
	 */
	static <E extends Enum<E> & EntityEnum<T, E>, T> E getEnum(Class<? extends EntityEnum<T, E>> clazz, T dbData) {
		return Arrays.stream(clazz.getEnumConstants()).filter(e -> e.equalsByDbData(dbData)).map(EntityEnum::toEnum)
				.findFirst().orElse(null);
	}

	/**
	 * [staticメソッド] 指定されたCodeEnumに、指定されたコード値を持つ列挙子が存在するかチェックする
	 *
	 * @param clazz 本インターフェースを実装した指定されたEnum
	 * @param dbData 本インターフェースを実装した指定されたEnumで定義されているDB値
	 * @param <T> DB値の型
	 * @param <E> 本インターフェースの実装Enumの型
	 * @return チェック結果
	 */
	static <E extends Enum<E> & EntityEnum<T, E>, T> boolean hasCode(Class<? extends EntityEnum<T, E>> clazz, T dbData) {
		return Arrays.stream(clazz.getEnumConstants()).anyMatch(e -> e.equalsByDbData(dbData));
	}
}

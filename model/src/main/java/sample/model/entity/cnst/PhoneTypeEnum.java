package sample.model.entity.cnst;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 連絡先種別Enum
 */
@AllArgsConstructor
@Getter
public enum PhoneTypeEnum implements EntityEnum<String, PhoneTypeEnum> {

	/** なし */
	NONE("none"),
	/** 自宅 */
	HOUSE("house"),
	/** 携帯 */
	MOBILE("mobile");

	/** DB値 */
	private String dbData;
}

package sample.model.entity.cnst;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 性別Enum
 */
@AllArgsConstructor
@Getter
public enum GenderEnum implements EntityEnum<String, GenderEnum> {

	/** 男性 */
	MALE("male"),
	/** 女性 */
	FEMALE("female");

	/** DB値 */
	private String dbData;
}

package sample.model.entity.cnst;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 注文ステータスEnum
 */
@AllArgsConstructor
@Getter
public enum OrderStatusEnum implements EntityEnum<String, OrderStatusEnum> {

	/** 新規作成 */
	CREATE("create"),
	/** 受付済み */
	ACCEPT("accept");

	/** DB値 */
	private String dbData;
}

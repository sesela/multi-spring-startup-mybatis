package sample.model.entity.cnst;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 権限Enum
 */
@AllArgsConstructor
@Getter
public enum CustomerRoleEnum implements EntityEnum<String, CustomerRoleEnum> {

	/** 管理者 */
	ADMIN("admin"),
	/** 一般ユーザ */
	NORMAL("normal");

	/** DB値 */
	private String dbData;
}

package sample.model.entity;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * カテゴリ情報
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Category implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** カテゴリコード */
	@NotEmpty
	@Size(max = 20)
	@Setter(value = AccessLevel.PRIVATE)
	private String code;

	/** カテゴリ名 */
	@NotEmpty
	@Size(max = 20)
	private String name;

	/** 内容 */
	@NotEmpty
	@Size(max = 255)
	private String description;

	/**
	 * コンストラクタ
	 * @param code カテゴリコード
	 * @param name カテゴリ名
	 * @param description 内容
	 */
	public Category(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}
}

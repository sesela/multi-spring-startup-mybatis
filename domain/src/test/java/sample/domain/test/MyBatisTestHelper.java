package sample.domain.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import sample.domain.test.EntityInfoTestConst.EntityInfo;

@Component
@AllArgsConstructor
@SuppressWarnings("serial")
public class MyBatisTestHelper {

	@Autowired
	private final NamedParameterJdbcTemplate jdbcTemplate;

	public List<Map<String, Object>> findAll(EntityInfo entityInfo) {
		return jdbcTemplate.query("SELECT * FROM " + entityInfo.tableName, new HashMap<String, Object>(), entityInfo.rowMapper);
	}

	public List<Map<String, Object>> findByKey(EntityInfo entityInfo, String columnName, Object value) {
		Map<String, Object> param = new HashMap<String, Object>() {
			{
				put(columnName, value);
			}
		};
		return jdbcTemplate.query(
				"SELECT * FROM " + entityInfo.tableName + " WHERE " + columnName + " = :" + columnName,
				param,
				entityInfo.rowMapper);
	}

	public void truncate(List<EntityInfo> entityInfoList) {
		entityInfoList.forEach(entityInfo -> jdbcTemplate.update(
				"TRUNCATE TABLE " + entityInfo.tableName,
				new HashMap<String, Object>()));
	}

}

package sample.domain.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * DBテーブル確認用テーブル定義クラス</br>
 * テーブル定義変更時にこのクラスに追加編集してください。
 */
@SuppressWarnings("serial")
public class EntityInfoTestConst {
	
	@AllArgsConstructor
	@Getter
	public static class EntityInfo {
		String tableName;
		RowMapper<Map<String, Object>> rowMapper;
	}
	
	public static final EntityInfo CUSTOMER = new EntityInfo(
			"t_customer",
			(rs, i) -> new HashMap<String, Object>() {
				{
					put("id", rs.getLong("id"));
					put("login_id", rs.getString("login_id"));
					put("login_password", rs.getString("login_password"));
					put("email", rs.getString("email"));
					put("last_name", rs.getString("last_name"));
					put("first_name", rs.getString("first_name"));
					put("gender", rs.getString("gender"));
					put("prefecture", rs.getString("prefecture"));
					put("city", rs.getString("city"));
					put("street", rs.getString("street"));
					put("house_no", rs.getString("house_no"));
					put("zip_code", rs.getString("zip_code"));
					put("phone_type", rs.getString("phone_type"));
					put("phone_no", rs.getString("phone_no"));
					put("created_by", rs.getString("created_by"));
					put("created_date", rs.getDate("created_date"));
					put("modified_by", rs.getString("modified_by"));
					put("modified_date", rs.getDate("modified_date"));
				}
			});
	
	public static final EntityInfo CUSTOMER_ROLE = new EntityInfo(
			"t_customer_role",
			(rs, i) -> new HashMap<String, Object>() {
				{
					put("id", rs.getLong("id"));
					put("customer_id", rs.getLong("customer_id"));
					put("role", rs.getString("role"));
					put("created_by", rs.getString("created_by"));
					put("created_date", rs.getDate("created_date"));
					put("modified_by", rs.getString("modified_by"));
					put("modified_date", rs.getDate("modified_date"));
				}
			});
	
	public static final EntityInfo CATEGORY = new EntityInfo(
			"m_category",
			(rs, i) -> new HashMap<String, Object>() {
				{
					put("code", rs.getString("code"));
					put("name", rs.getString("name"));
					put("description", rs.getString("description"));

				}
			});
	
	public static final EntityInfo ITEM = new EntityInfo(
			"m_item",
			(rs, i) -> new HashMap<String, Object>() {
				{
					put("code", rs.getString("code"));
					put("name", rs.getString("name"));
					put("description", rs.getString("description"));

				}
			});
	
	public static final EntityInfo ITEM_CATEGORY = new EntityInfo(
			"m_item_category",
			(rs, i) -> new HashMap<String, Object>() {
				{
					put("item_code", rs.getString("item_code"));
					put("category_code", rs.getString("category_code"));
				}
			});
	
}

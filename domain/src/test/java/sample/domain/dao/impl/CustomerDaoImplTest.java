package sample.domain.dao.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ValidationException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.DomainTestConfig;
import sample.domain.dao.CustomerDao;
import sample.domain.test.EntityInfoTestConst;
import sample.domain.test.MyBatisTestHelper;
import sample.model.entity.Customer;
import sample.model.entity.cnst.CustomerRoleEnum;
import sample.model.entity.cnst.GenderEnum;
import sample.model.entity.cnst.PhoneTypeEnum;
import sample.model.entity.cnst.PrefectureEnum;

@ExtendWith(SpringExtension.class)
class CustomerDaoImplTest {

	@Nested
	@ContextConfiguration(classes = DomainTestConfig.class)
	@MybatisTest
	@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
	@EnableAspectJAutoProxy(proxyTargetClass=true)
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@DisplayName("insertテスト")
	class Insert {
		@Autowired
		private MyBatisTestHelper myBatisTestHelper;
		@Autowired
		private CustomerDao customerDao;
		/** 入力データ */
		private Customer validData;
		
		@BeforeEach
		@AfterEach
		void setup() {
			myBatisTestHelper.truncate(Arrays.asList(EntityInfoTestConst.CUSTOMER));
			myBatisTestHelper.truncate(Arrays.asList(EntityInfoTestConst.CUSTOMER_ROLE));
			Set<CustomerRoleEnum> customerRoleSet = new HashSet<CustomerRoleEnum>();
			customerRoleSet.add(CustomerRoleEnum.NORMAL);
			customerRoleSet.add(CustomerRoleEnum.ADMIN);
			validData = new Customer("loginId", "password", "email@email.com", customerRoleSet);
			validData.setLastName("lastName");
			validData.setFirstName("firstName");
			validData.setGender(GenderEnum.MALE);
			validData.setPrefecture(PrefectureEnum.TOKYO);
			validData.setCity("city");
			validData.setAddressLine1("addressLine1");
			validData.setAddressLine2("addressLine2");
			validData.setZipCode("zipCode");
			validData.setPhoneType(PhoneTypeEnum.HOUSE);
			validData.setPhoneNo("phoneNo");
		}
		
		@Test
		@DisplayName("正常に登録できること")
		void valid() {
			customerDao.insert(validData);

			Map<String, Object> resultCustomer =  myBatisTestHelper.findByKey(EntityInfoTestConst.CUSTOMER, "id", validData.getId()).get(0);
			assertThat(resultCustomer.get("id")).isEqualTo(validData.getId());
			assertThat(resultCustomer.get("login_id")).isEqualTo(validData.getLoginId());
			assertThat(resultCustomer.get("login_password")).isEqualTo(validData.getLoginPassword());
			assertThat(resultCustomer.get("email")).isEqualTo(validData.getEmail());
			assertThat(resultCustomer.get("last_name")).isEqualTo(validData.getLastName());
			assertThat(resultCustomer.get("first_name")).isEqualTo(validData.getFirstName());
			assertThat(resultCustomer.get("gender")).isEqualTo(validData.getGender().getDbData());
			assertThat(resultCustomer.get("prefecture")).isEqualTo(validData.getPrefecture().getDbData());
			assertThat(resultCustomer.get("city")).isEqualTo(validData.getCity());
			assertThat(resultCustomer.get("street")).isEqualTo(validData.getAddressLine1());
			assertThat(resultCustomer.get("house_no")).isEqualTo(validData.getAddressLine2());
			assertThat(resultCustomer.get("zip_code")).isEqualTo(validData.getZipCode());
			assertThat(resultCustomer.get("phone_type")).isEqualTo(validData.getPhoneType().getDbData());
			assertThat(resultCustomer.get("phone_no")).isEqualTo(validData.getPhoneNo());
			assertThat(resultCustomer.get("created_by")).isEqualTo("system");
			assertThat(resultCustomer.get("created_date")).isNotNull();
			assertThat(resultCustomer.get("modified_by")).isNull();
			assertThat(resultCustomer.get("modified_date")).isNull();

			List<Map<String, Object>> resultCustomerRoleList =  myBatisTestHelper.findAll(EntityInfoTestConst.CUSTOMER_ROLE);			
			assertThat(resultCustomerRoleList.size()).isEqualTo(validData.getCustomerRoleSet().size());
			for(Map<String, Object> resultCustomerRole : resultCustomerRoleList) {
				assertThat(resultCustomerRole.get("id")).isNotNull();
				assertThat(resultCustomerRole.get("customer_id")).isEqualTo(validData.getId());
				assertThat(resultCustomerRole.get("role")).isIn(validData.getCustomerRoleSet().stream().map(v -> v.getDbData()).collect(Collectors.toList()));
				assertThat(resultCustomer.get("created_by")).isEqualTo("system");
				assertThat(resultCustomer.get("created_date")).isNotNull();
				assertThat(resultCustomer.get("modified_by")).isNull();
				assertThat(resultCustomer.get("modified_date")).isNull();
			}
		}
	
		@Test
		@DisplayName("メールアドレスフォーマットが正しくないときValidationExceptionが発生すること")
		void invalid() {
			Customer invalidData = validData;
			invalidData.setEmail("mail");
			assertThrows(ValidationException.class, () -> customerDao.insert(invalidData));
		}
	}

	@Nested
	@ContextConfiguration(classes = DomainTestConfig.class)
	@MybatisTest
	@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
	@EnableAspectJAutoProxy(proxyTargetClass=true)
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@DisplayName("updateテスト")
	class Update {
		//未実装（Insertテストを参考に実装）
	}

	@Nested
	@ContextConfiguration(classes = DomainTestConfig.class)
	@MybatisTest
	@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
	@EnableAspectJAutoProxy(proxyTargetClass=true)
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@DisplayName("deleteテスト")
	class Delete {
		//未実装（Insertテストを参考に実装）
	}

}

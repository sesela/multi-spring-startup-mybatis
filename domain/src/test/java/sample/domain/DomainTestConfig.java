package sample.domain;

import java.sql.Driver;
import java.util.Properties;

import javax.sql.DataSource;

import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import sample.domain.audit.AuditorAware;

@ComponentScan(basePackages = "sample.domain")
@MapperScan("sample.domain.mapper")
@Slf4j
public class DomainTestConfig {

	@Bean
	AuditorAware auditorAware() {
		return () -> "system";
	}

	@Bean
	ConfigurationCustomizer mybatisConfigurationCustomizer() {
		return (config) -> {
			config.getTypeAliasRegistry().registerAliases("sample.model.entity");
			config.getTypeHandlerRegistry().register("sample.model.entity.type");
		};
	}


	@SuppressWarnings("unchecked")
	@Bean
	@Profile("mysql")
	public DataSource dataSource() throws ClassNotFoundException {

		log.info("**** Setup MySQL DataSource");
		Properties sysProperties = System.getProperties();
		String mysqlUrl = sysProperties.getProperty("mysql.url");
		mysqlUrl = StringUtils.isEmpty(mysqlUrl) ? "jdbc:mysql://localhost:3306/sandbox?useUnicode=true&characterEncoding=utf8" : mysqlUrl;
		String mysqlUsername = sysProperties.getProperty("mysql.username");
		mysqlUsername = StringUtils.isEmpty(mysqlUsername) ? "root" : mysqlUsername;
		String mysqlPassword = sysProperties.getProperty("mysql.password");
		mysqlPassword = StringUtils.isEmpty(mysqlPassword) ?  "example" : mysqlPassword;
		log.info("MySQL url     : " + mysqlUrl);
		log.info("MySQL username: " + mysqlUsername);
		log.info("MySQL password: " + mysqlPassword);

		// ★★★ Springが提供しているコネクションをプールしない実装クラスを利用 ★★★
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriverClass((Class<? extends Driver>) Class.forName("com.mysql.cj.jdbc.Driver"));
		dataSource.setUrl(mysqlUrl);
		dataSource.setUsername(mysqlUsername);
		dataSource.setPassword(mysqlPassword);
		Properties connectionProperties = new Properties();
		connectionProperties.setProperty("autoCommit", "false"); // ★★★ 自動コミットをOFFにする ★★★
		dataSource.setConnectionProperties(connectionProperties);

		// schema init
		// https://stackoverflow.com/questions/38040572/spring-boot-loading-initial-data
		Resource initSchema = new ClassPathResource("schema.sql");
		Resource initData = new ClassPathResource("data.sql");
		DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema, initData);
		DatabasePopulatorUtils.execute(databasePopulator, dataSource);
		
		return dataSource;
	}
}

package sample.domain.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.HashSet;

import javax.validation.ValidationException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.DomainTestConfig;
import sample.domain.mapper.CustomerMapper;
import sample.domain.service.CustomerService;
import sample.domain.test.EntityInfoTestConst;
import sample.domain.test.MyBatisTestHelper;
import sample.model.entity.Customer;

@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {
	
	@Nested
	@DisplayName("createテスト")
	@MybatisTest
	@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
	@EnableAspectJAutoProxy(proxyTargetClass=true)
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@ContextConfiguration(classes = DomainTestConfig.class)
	class CreateTest {
		@Autowired
		private MyBatisTestHelper myBatisTestHelper;
		@Autowired
		private CustomerService customerService;
		@Autowired
		private CustomerMapper customerMapper;

		@BeforeEach
		@AfterEach
		void setup() {
			myBatisTestHelper.truncate(Arrays.asList(EntityInfoTestConst.CUSTOMER));
			myBatisTestHelper.truncate(Arrays.asList(EntityInfoTestConst.CUSTOMER_ROLE));
		}
		
		@Test
		@DisplayName("正常に登録されること")
		void valid() {
			Customer customer = new Customer("loginId", "loginPassword", "email@email.com", new HashSet<>());
			customerService.create(customer);
			assertThat(customerMapper.findOne(customer.getId())).isNotNull();
		}
		@Test
		@DisplayName("例外発生時にロールバックされること")
		void invalid() {
			Customer customer = new Customer("loginId", "loginPassword", "email", new HashSet<>());
			assertThrows(ValidationException.class, () -> customerService.create(customer));
			assertThat(customerMapper.findOne(customer.getId())).isNull();
		}

	}
}

package sample.domain.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.DomainTestConfig;
import sample.domain.dao.CustomerDao;
import sample.domain.test.EntityInfoTestConst;
import sample.domain.test.MyBatisTestHelper;
import sample.model.entity.Customer;
import sample.model.entity.cnst.CustomerRoleEnum;
import sample.model.entity.cnst.GenderEnum;
import sample.model.entity.cnst.PhoneTypeEnum;
import sample.model.entity.cnst.PrefectureEnum;

@ExtendWith(SpringExtension.class)
class CustomerMapperTest {

	@Nested
	@ContextConfiguration(classes = DomainTestConfig.class)
	@MybatisTest
	@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
	@EnableAspectJAutoProxy(proxyTargetClass=true)
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	@DisplayName("findOneテスト")
	class FindOneTest {
		@Autowired
		private CustomerDao customerDao;
		@Autowired
		private MyBatisTestHelper myBatisTestHelper;
		@Autowired
		private CustomerMapper customerMapper;
		/** 入力データ */
		private Customer validData;

		@BeforeEach
		@AfterEach
		void setup() {
			myBatisTestHelper.truncate(Arrays.asList(EntityInfoTestConst.CUSTOMER));
			myBatisTestHelper.truncate(Arrays.asList(EntityInfoTestConst.CUSTOMER_ROLE));
			Set<CustomerRoleEnum> customerRoleSet = new HashSet<CustomerRoleEnum>();
			customerRoleSet.add(CustomerRoleEnum.NORMAL);
			customerRoleSet.add(CustomerRoleEnum.ADMIN);
			validData = new Customer("loginId", "password", "email@email.com", customerRoleSet);
			validData.setLastName("lastName");
			validData.setFirstName("firstName");
			validData.setGender(GenderEnum.MALE);
			validData.setPrefecture(PrefectureEnum.TOKYO);
			validData.setCity("city");
			validData.setAddressLine1("addressLine1");
			validData.setAddressLine2("addressLine2");
			validData.setZipCode("zipCode");
			validData.setPhoneType(PhoneTypeEnum.HOUSE);
			validData.setPhoneNo("phoneNo");
		}
		
		@Test
		@DisplayName("データが取得できること")
		void found() {
			customerDao.insert(validData);
			
			Customer customer = customerMapper.findOne(validData.getId());
			assertThat(customer.getId()).isEqualTo(validData.getId());
			assertThat(customer.getLoginId()).isEqualTo(validData.getLoginId());
			assertThat(customer.getLoginPassword()).isEqualTo(validData.getLoginPassword());
			assertThat(customer.getEmail()).isEqualTo(validData.getEmail());
			assertThat(customer.getLastName()).isEqualTo(validData.getLastName());
			assertThat(customer.getFirstName()).isEqualTo(validData.getFirstName());
			assertThat(customer.getGender()).isEqualTo(validData.getGender());
			assertThat(customer.getPrefecture()).isEqualTo(validData.getPrefecture());
			assertThat(customer.getCity()).isEqualTo(validData.getCity());
			assertThat(customer.getAddressLine1()).isEqualTo(validData.getAddressLine1());
			assertThat(customer.getAddressLine2()).isEqualTo(validData.getAddressLine2());
			assertThat(customer.getZipCode()).isEqualTo(validData.getZipCode());
			assertThat(customer.getPhoneType()).isEqualTo(validData.getPhoneType());
			assertThat(customer.getPhoneNo()).isEqualTo(validData.getPhoneNo());
			assertThat(customer.getCustomerRoleSet()).isEqualTo(validData.getCustomerRoleSet());
		}

		@Test
		@DisplayName("取得対象が存在しない場合はnullを返すこと")
		void notFound() {
			customerDao.insert(validData);
			Customer customer = customerMapper.findOne(-999L);
			assertThat(customer).isNull();
		}
	}
}

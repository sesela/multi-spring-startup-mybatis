DROP TABLE IF EXISTS t_customer_role;
DROP TABLE IF EXISTS t_customer;
DROP TABLE IF EXISTS m_category;
DROP TABLE IF EXISTS m_item;
DROP TABLE IF EXISTS m_item_category;

CREATE TABLE t_customer (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	login_id VARCHAR(20) NOT NULL,
	login_password VARCHAR(255) NOT NULL,
	email VARCHAR(100) NOT NULL,
	last_name VARCHAR(20),
	first_name VARCHAR(20),
    gender VARCHAR(20),
    prefecture VARCHAR(20),
	city VARCHAR(20),
	street VARCHAR(100),
	house_no VARCHAR(100),
	zip_code VARCHAR(10),
	phone_type VARCHAR(10),
	phone_no VARCHAR(10),
    created_by VARCHAR(20) NOT NULL,
    created_date DATETIME NOT NULL,
    modified_by VARCHAR(20),
    modified_date DATETIME
);

CREATE TABLE t_customer_role (
	id BIGINT PRIMARY KEY AUTO_INCREMENT,
	customer_id BIGINT NOT NULL,
	role VARCHAR(20) NOT NULL,
    created_by VARCHAR(20) NOT NULL,
    created_date DATETIME NOT NULL,
    modified_by VARCHAR(20),
    modified_date DATETIME
) engine = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE m_category (
    code VARCHAR(20) PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE m_item (
    code VARCHAR(20) PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE m_item_category (
    item_code VARCHAR(20),
    category_code VARCHAR(20),
    PRIMARY KEY(item_code,category_code)
);
package sample.domain.dao;

import sample.model.entity.Item;

/**
 * 商品情報データアクセスラッパーインターフェース
 */
public interface ItemDao {

	/**
	 * 商品情報を登録します。
	 * @param item 商品情報
	 */
	void insert(Item item);
	/**
	 * 商品情報を更新します。
	 * @param item 商品情報
	 */
	void update(Item item);
	/**
	 * 商品情報を削除します。
	 * @param item 商品情報
	 */
	void delete(Item item);
}

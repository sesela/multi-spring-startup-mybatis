package sample.domain.dao;

import sample.model.entity.Category;

/**
 * カテゴリ情報データアクセスラッパーインターフェース
 */
public interface CategoryDao {

	/**
	 * カテゴリ情報を登録します。
	 * @param category カテゴリ情報
	 */
	void insert(Category category);
	/**
	 * カテゴリ情報を更新します。
	 * @param category カテゴリ情報
	 */
	void update(Category category);
	/**
	 * カテゴリ情報を削除します。
	 * @param category カテゴリ情報
	 */
	void delete(Category category);

}

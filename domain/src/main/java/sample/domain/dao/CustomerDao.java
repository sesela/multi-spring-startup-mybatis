package sample.domain.dao;

import sample.model.entity.Customer;

/**
 * 顧客情報データアクセスラッパーインターフェース
 */
public interface CustomerDao {

	/**
	 * 顧客情報を登録します。
	 * @param customer 顧客情報
	 */
	void insert(Customer customer);
	/**
	 * 顧客情報を更新します。
	 * @param customer 顧客情報
	 */
	void update(Customer customer);
	/**
	 * 顧客情報を削除します。
	 * @param customer 顧客情報
	 */
	void delete(Customer customer);

}

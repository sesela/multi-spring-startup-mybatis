package sample.domain.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import sample.common.exception.BusinessLogicException;
import sample.domain.dao.ItemDao;
import sample.model.entity.Category;
import sample.model.entity.Item;

/**
 * 商品情報データアクセスラッパー実装クラス
 */
@Component
@AllArgsConstructor
@Transactional(readOnly = true)
public class ItemDaoImpl implements ItemDao {

	/** SqlSession */
	private final SqlSession sqlSession;

	/**
	 * {@inheritDoc}
	 */
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	@Override
	public void insert(Item item) {
		sqlSession.insert("sample.domain.mapper.ItemMapper.insert", item);
		for (Category category : item.getCategorySet()) {
			Map<String, Object> param = new HashMap<>();
			param.put("itemCode", item.getCode());
			param.put("categoryCode", category.getCode());
			sqlSession.insert("sample.domain.mapper.ItemMapper.insertItemCategory", param);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void update(Item item) {
		if (sqlSession.update("sample.domain.mapper.ItemMapper.update", item) != 1) {
			throw new BusinessLogicException("更新処理に失敗しました。" + item);
		}
		sqlSession.delete("sample.domain.mapper.ItemMapper.deleteItemCategoryByItemCode", item.getCode());
		item.getCategorySet().forEach((category) -> {
			Map<String, Object> param = new HashMap<>();
			param.put("itemCode", item.getCode());
			param.put("categoryCode", category.getCode());
			sqlSession.insert("sample.domain.mapper.ItemMapper.insertItemCategory", param);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void delete(Item item) {
		sqlSession.delete("sample.domain.mapper.ItemMapper.deleteItemCategoryByItemCode", item.getCode());
		if (sqlSession.delete("sample.domain.mapper.ItemMapper.delete", item.getCode()) != 1) {
			throw new BusinessLogicException("削除処理に失敗しました。" + item);
		}
	}
}

package sample.domain.dao.impl;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import sample.common.exception.BusinessLogicException;
import sample.domain.dao.CategoryDao;
import sample.model.entity.Category;

/**
 * カテゴリ情報データアクセスラッパー実装クラス
 */
@Component
@AllArgsConstructor
@Transactional(readOnly = true)
public class CategoryDaoImpl implements CategoryDao {

	/** SqlSession */
	private final SqlSession sqlSession;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void insert(Category category) {
		sqlSession.insert("sample.domain.mapper.CategoryMapper.insert", category);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void update(Category category) {
		if (sqlSession.update("sample.domain.mapper.CategoryMapper.update", category) != 1) {
			throw new BusinessLogicException("更新処理に失敗しました。" + category);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void delete(Category category) {
		if (sqlSession.delete("sample.domain.mapper.CategoryMapper.delete", category.getCode()) != 1) {
			throw new BusinessLogicException("削除処理に失敗しました。" + category);
		}
	}
}

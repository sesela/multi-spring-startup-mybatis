package sample.domain.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import sample.common.exception.BusinessLogicException;
import sample.domain.dao.CustomerDao;
import sample.model.entity.Customer;

/**
 * 顧客情報データアクセスラッパー実装クラス
 */
@Component
@AllArgsConstructor
@Transactional(readOnly = true)
public class CustomerDaoImpl implements CustomerDao {

	/** SqlSession */
	private final SqlSession sqlSession;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void insert(Customer customer) {
		sqlSession.insert("sample.domain.mapper.CustomerMapper.insert", customer);
		customer.getCustomerRoleSet().forEach((role) -> {
			Map<String, Object> param = new HashMap<>();
			param.put("customerId", customer.getId());
			param.put("role", role);
			sqlSession.insert("sample.domain.mapper.CustomerMapper.insertRole", param);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void update(Customer customer) {
		if (sqlSession.update("sample.domain.mapper.CustomerMapper.update", customer) != 1) {
			throw new BusinessLogicException("更新処理に失敗しました。" + customer);
		}
		sqlSession.delete("sample.domain.mapper.CustomerMapper.deleteRoleByCustomerId", customer.getId());
		customer.getCustomerRoleSet().forEach((role) -> {
			Map<String, Object> param = new HashMap<>();
			param.put("customerId", customer.getId());
			param.put("role", role);
			sqlSession.insert("sample.domain.mapper.CustomerMapper.insertRole", param);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void delete(Customer customer) {
		sqlSession.delete("sample.domain.mapper.CustomerMapper.deleteRoleByCustomerId", customer.getId());
		if (sqlSession.delete("sample.domain.mapper.CustomerMapper.delete", customer.getId()) != 1) {
			throw new BusinessLogicException("削除処理に失敗しました。" + customer);
		}
	}
}

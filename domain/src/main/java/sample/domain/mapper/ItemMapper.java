package sample.domain.mapper;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import sample.model.entity.Item;

/**
 * 商品情報データアクセス
 */
public interface ItemMapper {

	/**
	 * 商品情報を引数で指定した条件で取得します。
	 * @param code 商品コード
	 * @return 商品情報
	 */
	Item findOne(String code);

	/**
	 * 全商品情報を取得します。
	 * @return 商品情報
	 */
	List<Item> findAll();

	/**
	 * 全商品情報を取得します。
	 * @param rowBounds RowBounds
	 * @return 商品情報
	 */
	List<Item> findAll(RowBounds rowBounds);

	/**
	 * 指定した商品コード存在有無チェック
	 * @param code 商品コード
	 * @return チェック結果
	 */
	boolean existsByCode(String code);
}

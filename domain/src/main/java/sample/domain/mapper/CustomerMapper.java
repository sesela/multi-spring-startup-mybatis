package sample.domain.mapper;

import java.util.List;

import sample.model.entity.Customer;

/**
 * 顧客情報データアクセス
 */
public interface CustomerMapper {

	/**
	 * 顧客情報を引数で指定した条件で取得します。
	 * @param id 顧客ID
	 * @return 顧客情報
	 */
	Customer findOne(Long id);

	/**
	 * 顧客情報を引数で指定した条件で取得します。
	 * @param loginId ログインID
	 * @return 顧客情報
	 */
	List<Customer> findByLoginId(String loginId);

	/**
	 * 全顧客情報を取得します。
	 * @return 顧客情報
	 */
	List<Customer> findAll();

	/**
	 * 指定したログインID存在有無チェック
	 * @param loginId ログインID
	 * @return チェック結果
	 */
	boolean existsByLoginId(String loginId);

	/**
	 * 指定したメールアドレス存在有無チェック
	 * @param email メールアドレス
	 * @return チェック結果
	 */
	boolean existsByEmail(String email);
}

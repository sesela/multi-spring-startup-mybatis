package sample.domain.mapper;

import java.util.List;

import org.apache.ibatis.session.RowBounds;

import sample.model.entity.Category;

/**
 * カテゴリ情報データアクセス
 */
public interface CategoryMapper {

	/**
	 * カテゴリ情報を引数で指定した条件で取得します。
	 * @param code カテゴリコード
	 * @return カテゴリ情報
	 */
	Category findOne(String code);

	/**
	 * 全カテゴリ情報を取得します。
	 * @return カテゴリ情報
	 */
	List<Category> findAll();

	/**
	 * 全カテゴリ情報を取得します。
	 * @param rowBounds RowBounds
	 * @return カテゴリ情報
	 */
	List<Category> findAll(RowBounds rowBounds);

	/**
	 * 指定したカテゴリコード存在有無チェック
	 * @param code カテゴリコード
	 * @return チェック結果
	 */
	boolean existsByCode(String code);
}

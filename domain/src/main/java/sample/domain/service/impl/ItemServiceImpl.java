package sample.domain.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import sample.domain.dao.ItemDao;
import sample.domain.mapper.ItemMapper;
import sample.domain.service.ItemService;
import sample.model.entity.Item;

/**
 * 商品サービス実装クラス
 */
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class ItemServiceImpl implements ItemService {

	/** ItemMapper */
	private final ItemMapper itemMapper;
	/** ItemDao */
	private final ItemDao itemDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void create(Item item) {
		itemDao.insert(item);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void update(Item item) {
		itemDao.update(item);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void delete(Item item) {
		itemDao.delete(item);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Item findByCode(String code) {
		return itemMapper.findOne(code);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Item> findAll() {
		return itemMapper.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean existsByCode(String code) {
		return itemMapper.existsByCode(code);
	}
}

package sample.domain.service.impl;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import sample.common.exception.BusinessLogicException;
import sample.domain.dao.CustomerDao;
import sample.domain.mapper.CustomerMapper;
import sample.domain.service.CustomerService;
import sample.model.entity.Customer;

/**
 * 顧客サービス実装クラス
 */
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class CustomerServiceImpl implements CustomerService {

	/** CustomerMapper */
	private final CustomerMapper customerMapper;
	/** CustomerDao */
	private final CustomerDao customerDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void create(Customer customer) {
		customerDao.insert(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void update(Customer customer) {
		customerDao.update(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void delete(Customer customer) {
		customerDao.delete(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Customer> findAll() {
		return customerMapper.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer findById(long id) {
		return customerMapper.findOne(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Customer findByLonginId(String loginId) {
		List<Customer> result = customerMapper.findByLoginId(loginId);
		if (result.size() > 1) {
			throw new BusinessLogicException("found multiple customer. loginId:" + loginId);
		}
		return result.isEmpty() ? null : result.get(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean existsEmail(String email) {
		return customerMapper.existsByEmail(email);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean existsLoginId(String loginId) {
		return customerMapper.existsByLoginId(loginId);
	}
}

package sample.domain.service.impl;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import sample.domain.dao.CategoryDao;
import sample.domain.mapper.CategoryMapper;
import sample.domain.service.CategoryService;
import sample.model.entity.Category;

/**
 * カテゴリサービス実装åクラス
 */
@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class CategoryServiceImpl implements CategoryService {

	/** CategoryMapper */
	private final CategoryMapper categoryMapper;
	/** CategoryDao */
	private final CategoryDao categoryDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void create(Category category) {
		categoryDao.insert(category);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void update(Category category) {
		categoryDao.update(category);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void delete(Category category) {
		categoryDao.delete(category);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Category findByCode(String code) {
		return categoryMapper.findOne(code);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Category> findAll() {
		return categoryMapper.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Category> findAll(int offset, int limit) {
		return categoryMapper.findAll(new RowBounds(offset, limit));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean existsByCode(String code) {
		return categoryMapper.existsByCode(code);
	}

}

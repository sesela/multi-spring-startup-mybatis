package sample.domain.service;

import java.util.List;

import sample.model.entity.Category;

/**
 * カテゴリサービスインターフェース
 */
public interface CategoryService {

	/**
	 * カテゴリ情報を登録します。
	 * @param category カテゴリ情報
	 */
	void create(Category category);

	/**
	 * カテゴリ情報を更新します。
	 * @param category カテゴリ情報
	 */
	void update(Category category);

	/**
	 * カテゴリ情報を削除します。
	 * @param category カテゴリ情報
	 */
	void delete(Category category);

	/**
	 * カテゴリ情報を指定した条件で取得します。
	 * @param code カテゴリコード
	 * @return カテゴリ情報
	 */
	Category findByCode(String code);

	/**
	 * 全カテゴリ情報を取得します。
	 * @return カテゴリ情報
	 */
	List<Category> findAll();

	/**
	 * 全カテゴリ情報を取得します。
	 * @param offset オフセット(0~)
	 * @param limit 取得件数
	 * @return カテゴリ情報
	 */
	List<Category> findAll(int offset, int limit);

	/**
	 * カテゴリコード登録済みチェックを行います。
	 * @param code カテゴリコード
	 * @return true:登録済み, false:未登録
	 */
	boolean existsByCode(String code);
}

package sample.domain.service;

import java.util.List;

import sample.model.entity.Item;

/**
 * 商品サービスインターフェース
 */
public interface ItemService {

	/**
	 * 商品情報を登録します。
	 * @param item 商品情報
	 */
	void create(Item item);

	/**
	 * 商品情報を更新します。
	 * @param item 商品情報
	 */
	void update(Item item);

	/**
	 * 商品情報を削除します。
	 * @param item 商品情報
	 */
	void delete(Item item);

	/**
	 * 商品情報を指定した条件で取得します。
	 * @param code 商品コード
	 * @return 商品情報
	 */
	Item findByCode(String code);

	/**
	 * 全商品情報を取得します。
	 * @return 商品情報
	 */
	List<Item> findAll();

	/**
	 * 商品コード登録済みチェックを行います。
	 * @param code 商品コード
	 * @return true:登録済み, false:未登録
	 */
	boolean existsByCode(String code);

}

package sample.domain.service;

import java.util.List;

import sample.model.entity.Customer;

/**
 * 顧客サービスインターフェース
 */
public interface CustomerService {

	/**
	 * 顧客情報を登録します。
	 * @param customer 顧客情報
	 */
	void create(Customer customer);

	/**
	 * 顧客情報を更新します。
	 * @param customer 顧客情報
	 */
	void update(Customer customer);

	/**
	 * 顧客情報を削除します。
	 * @param customer 顧客情報
	 */
	void delete(Customer customer);

	/**
	 * 顧客情報を指定した条件で取得します。
	 * @param id 顧客ID
	 * @return 顧客情報
	 */
	Customer findById(long id);

	/**
	 * 全顧客情報を取得します。
	 * @return 顧客情報
	 */
	List<Customer> findAll();

	/**
	 * 顧客情報を指定した条件で取得します。
	 * @param loginId ログインID
	 * @return 顧客情報
	 */
	Customer findByLonginId(String loginId);

	/**
	 * メールアドレス登録済みチェックを行います。
	 * @param email メールアドレス
	 * @return true:登録済み, false:未登録
	 */
	boolean existsEmail(String email);

	/**
	 * ログインID登録済みチェックを行います。
	 * @param loginId ログインID
	 * @return true:登録済み, false:未登録
	 */
	boolean existsLoginId(String loginId);
}

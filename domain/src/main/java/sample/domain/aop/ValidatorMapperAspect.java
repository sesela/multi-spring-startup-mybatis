package sample.domain.aop;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * DBアクセス時のバリデーションチェック処理クラス
 */
@Aspect
@Component
public class ValidatorMapperAspect {

	/**
	 * 登録、更新前にバリデーションチェックを行います。
	 * @param jp JoinPoint
	 * @throws ValidationException バリデーション例外
	 */
	@Before(
			"execution(* sample.domain.mapper.*Mapper.insert*(..))"
			+ " || execution(* sample.domain.mapper.*Mapper.create*(..))"
			+ " || execution(* sample.domain.mapper.*Mapper.update*(..))"
			+ " || execution(* sample.domain.dao.*Dao.insert*(..))"
			+ " || execution(* sample.domain.dao.*Dao.create*(..))"
			+ " || execution(* sample.domain.dao.*Dao.update*(..))"
			)
	public void validate(JoinPoint jp) throws ValidationException {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		List<ConstraintViolation<Object>> resultList = new ArrayList<>();
		for (Object obj : jp.getArgs()) {
			resultList.addAll(validator.validate(obj));
		}
		if (!resultList.isEmpty()) {
			throw new ValidationException("invalid error: " + resultList);
		}
	}

}

package sample.domain.audit;

import org.springframework.stereotype.Component;

/**
 * 監査メタ情報マッパーヘルパークラス
 */
@Component
public final class AuditorAwareHelper {

	/** 監査メタ情報 */
	private static AuditorAware auditorAware;

	/**
	 * コンストラクタ
	 * @param auditorAware 監査メタ情報
	 */
	private AuditorAwareHelper(AuditorAware auditorAware) {
		AuditorAwareHelper.setAuditorAware(auditorAware);
	}

	/**
	 * 監査メタ情報を設定します。
	 * @param auditorAware 監査メタ情報
	 */
	static synchronized void setAuditorAware(AuditorAware auditorAware) {
		AuditorAwareHelper.auditorAware = auditorAware;
	}

	/**
	 * 監査人を取得します。
	 * @return 監査人
	 */
	public static Object getCurrentAuditor() {
		return auditorAware.getCurrentAuditor();
	}

	/**
	 * 現在の時間を取得します。
	 * @return 現在の時間
	 */
	public static Object getCurrentDatetime() {
		return auditorAware.getCurrentDatetime();
	}
}

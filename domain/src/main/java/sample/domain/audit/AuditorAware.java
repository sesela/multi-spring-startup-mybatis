package sample.domain.audit;

import java.util.Date;

/**
 * 監査メタ情報インターフェース
 */
public interface AuditorAware {

	/**
	 * 監査人を取得します。
	 * @return 監査人
	 */
	Object getCurrentAuditor();

	/**
	 * 現在の時間を取得します。
	 * @return 現在の時間
	 */
	default Object getCurrentDatetime() {
		return new Date();
	}
}
